#include "provider.hh"

void user_b(void);

int main(int, char **) {
  user_b();
  Provided p;
  p.noop();
  return 0;
}
