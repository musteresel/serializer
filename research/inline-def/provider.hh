
struct Provided {
  int x;
  void noop(void) const;
};

/** inline allows definition in header file */
inline void Provided::noop() const {
}


