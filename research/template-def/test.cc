template<typename X> struct S {
  X x;
  bool is(void) const;
};

/** template<typename X> AND S<X> are required.
 * Therefore, the exact template decl has to be generated. That is
 * the number AND type, not the name (that can be generated).
 * */
template<typename Y> bool S<Y>::is() const {
}


int main(int, char**) {
  return 0;
}
