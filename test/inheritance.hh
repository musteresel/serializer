class Base {
  int dummy;
};
class Derived : Base {
  bool value;
};
class MoreDerived : Derived {
  char c;
};
class OtherBase {
  void * ptr;
};
class DerivesTwo : Base, OtherBase {
  long long longlong;
};
class DerivesVirt : virtual Base {
};
class DerivesVirtToo : virtual Base {
};
class Diamond : DerivesVirt, DerivesVirtToo {
};
