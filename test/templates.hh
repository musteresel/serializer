template<typename X> struct T {
  X x;
};
template<typename Y> struct T<Y*> {
  Y y;
};
template<> struct T<bool> {
  bool a;
  bool b;
};
template<template<template<class> class, class, int> class X, typename T>
struct WOAH_HOLD_ON {
  bool insane;
};

template<typename X> struct DT : T<X*> {
  long longlongway;
};
