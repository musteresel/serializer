class A {
  private:
    int priv_a;
    const int priv_ca;
  protected:
    int prot_a;
    const int prot_ca;
  public:
    int pub_a;
    const int pub_ca;
};
