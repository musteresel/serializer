namespace N1 {
  struct InN1 {
    int dummy;
  };
  namespace N2 {
    struct InN2 {
      int dummy;
    };
    struct InN2Too {};
  }
  struct UsingFromN2 {
    N2::InN2 use;
  };
  struct DerivingFromN2 : public N2::InN2Too {};
}
struct Global : N1::InN1 {
  using namespace N1::N2;
  InN2 n2;
  N1::UsingFromN2 n1;
};
