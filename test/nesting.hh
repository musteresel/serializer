
struct Outer {
  struct Inner {
  };
};
struct Using {
  Outer::Inner use;
};
struct Deriving : Outer::Inner {};

template<typename T> struct TOut {
  struct In {};
  template<typename X> struct TIn {};
};
template<typename X, typename Y> struct TUsingT : TOut<X>::template TIn<Y> {};
