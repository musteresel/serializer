#ifndef _SERIALIZER_HH_
#define _SERIALIZER_HH_ 1
namespace serializer {
  template<typename T>
    std::size_t serialize(T const & obj, char * const buffer) {
      return obj.serialize(buffer);
    }
  template<typename T>
    std::size_t deserialize(T & obj, char const * const buffer) {
      return obj.deserialize(buffer);
    }
}
#endif

