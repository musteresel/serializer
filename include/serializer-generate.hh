#ifndef _SERIALIZER_GENERATE_HH_
#define _SERIALIZER_GENERATE_HH_ 1
#include <cstddef>
#define SERIALIZER_GENERATE \
  std::size_t serialize(char * const) const; \
  std::size_t deserialize(char const * const);
#endif

