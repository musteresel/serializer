#include <cstdint>
#include <serializer.hh>
namespace serializer {
  template<> std::size_t serialize<bool>(bool const & b, char * const buffer) {
    buffer[0] = b ? 'T' : 'F';
    buffer[1] = '\n';
    return 2;
  }
  template<> std::size_t serialize<int>(int const & i, char * const buffer) {
    char * write = buffer;
    for (std::size_t bit = 0; bit < 32; ++bit) {
      *write = (i & (1 << bit)) != 0 ? '1' : '0';
      ++write;
    }
    *write = '\n';
    ++write;
    return write - buffer;
  }
  template<>
    std::size_t deserialize<bool>(bool & b, char const * const buffer) {
    b = buffer[0] == 'T' ? true : false;
    return 2;
  }
  template<>
    std::size_t deserialize<int>(int & i, char const * const buffer) {
      char const * read = buffer;
      i = 0;
      for (std::size_t bit = 0; bit < 32; ++bit) {
        if (*read == '1') {
          i |= (1 << bit);
        }
        ++read;
      }
      ++read;
      return read - buffer;
    }
}

