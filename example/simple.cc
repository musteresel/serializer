#include "to_char_buffer.hh"
#include "simple-serializer.hh"
#include <iostream>

int main(int, char**) {
  char * buffer = new char[2000];
  Data d{42,{true, 21}};
  std::size_t num = serializer::serialize(d, buffer);
  buffer[num] = '\0';
  std::cout << buffer << std::endl;
  Data r;
  serializer::deserialize(r, buffer);
  num = serializer::serialize(r, buffer);
  buffer[num] = '\0';
  std::cout << buffer << std::endl;
  std::cout << "Equal? " << (r == d) << std::endl;
  delete [] buffer;
  return 0;
}
