#ifndef _SIMPLE_HH_
#define _SIMPLE_HH_ 1
#include <cstdint>

#define SERIALIZER_GENERATE_CODE \
  std::size_t serialize(char * const) const; \
  std::size_t deserialize(char const * const);

struct SubData {
  bool flag;
  int optional;
  SERIALIZER_GENERATE_CODE;
};

struct Data {
  int contents;
  SubData subdata;
  SERIALIZER_GENERATE_CODE;
  bool operator==(Data const & o) {
    return
      contents == o.contents and
      subdata.flag == o.subdata.flag and
      subdata.optional == o.subdata.optional;
  }
};

#endif

